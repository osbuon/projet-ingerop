\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english,main=french]{babel}
\usepackage[babel=true]{csquotes}
\usepackage{graphicx}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{algorithm2e}
\usepackage{hyperref}
\usepackage[toc,page]{appendix}
\usepackage[backend=biber,style=alphabetic]{biblatex}


\graphicspath{ {../Images/} }
\usetheme{Copenhagen}
\setbeamertemplate{page number in head/foot}[totalframenumber]


\title{Soutenance de projet de 2ème année}
\subtitle{Scan To Bim}
\author{Oscar Buon \and Duneshwar Badal}
\date{20 mars 2023}


\begin{document}

\begin{frame}
    \centering
    \begin{minipage}{0.3\textwidth}
        \includegraphics[width=\textwidth]{logo_ISIMA_INP.png}
    \end{minipage}\hfill
    \begin{minipage}{0.3\textwidth}
        \includegraphics[width=\textwidth]{logo_Ingerop.png}
    \end{minipage}

    \maketitle

    Jury: Baptiste Jacquouton, Mamadou Kanté, Murielle Mouzat
\end{frame}

\part{Introduction}
\section*{Introduction}

    \begin{frame}
        \includegraphics[width=\textwidth, center]{logo_Ingerop.png}
    \end{frame}

    \begin{frame}
        \includegraphics[width=\textwidth, center]{Photogrametrie.jpg}
    \end{frame}

    \part{Corps}

    \begin{frame}
        \frametitle{Table des matières}
        \tableofcontents
    \end{frame}

\section{Organisation du travail et outils}

    \begin{frame}
        \begin{LARGE}
            \centerline{Organisation du travail et outils}
        \end{LARGE}
    \end{frame}

    \subsection{Gantt}

        \begin{frame}
            \frametitle{Diagramme de Gantt prévisionnel}
            \includegraphics[width=\textwidth, center]{Gantt_previsionnel.png}
        \end{frame}

        \begin{frame}
            \frametitle{Diagramme de Gantt}
            \includegraphics[width=\textwidth, center]{Gantt.png}
        \end{frame}

    \subsection{Outils}

        \begin{frame}
            \frametitle{Unity}
            \includegraphics[width=\textwidth, center]{logo_Unity.png}
        \end{frame}

        \begin{frame}
            \frametitle{Nuage de points}
            \includegraphics[width=0.5\textwidth, center]{Point_cloud_torus-0.png}
        \end{frame}

\section{Travail effectué}

    \begin{frame}
        \begin{LARGE}
            \centerline{Travail effectué}
        \end{LARGE}
    \end{frame}

    \subsection{Ball Pivoting Algorithm}

        \begin{frame}
            \frametitle{Illustration du BPA en deux dimensions}
            \includegraphics[width=\textwidth, center]{BPA.png}
        \end{frame}

        \begin{frame}
            \frametitle{Limitations du BPA}
            \includegraphics[width=\textwidth, center]{BPA_limites.png}
        \end{frame}

        \begin{frame}
            \frametitle{Partionnement de l'espace}
            \includegraphics[width=0.5\textwidth, center]{Octree.png}
        \end{frame}

    \subsection{Triangulation de Delaunay}

        \begin{frame}
            \frametitle{Triangulation de Delaunay en deux dimensions}
            \includegraphics[width=\textwidth, center]{Delaunay.png}
        \end{frame}

        \begin{frame}
            \frametitle{Triangulation de Delaunay en trois dimensions}
            \includegraphics[width=0.75\textwidth, center]{Resultat_Dealunay.png}
        \end{frame}

\section{Résultats et critique}

    \begin{frame}
        \begin{LARGE}
            \centerline{Résultats et critique}
        \end{LARGE}
    \end{frame}

    \subsection{Chargement et affichage d'un nuage de points}

        \begin{frame}
            \frametitle{Nuage de points}
            \includegraphics[width=\textwidth, center]{Nuage.png}
        \end{frame}

    \subsection{Implémentation du Ball Pivoting Algorithm}

        \begin{frame}
            \frametitle{Cube construit via BPA}
            \includegraphics[width=0.65\textwidth, center]{Cube.png}
        \end{frame}

    \subsection{Limitation des outils}

        \begin{frame}
            \frametitle{Unity}

            \begin{itemize}
                \item Développement rapide
                \item Débuggage des algorithmes compliqué
                \item Algorithmes C\# lents à exécuter
            \end{itemize}
        \end{frame}

\part{Conclusion}
\section*{Conclusion}

    \begin{frame}
        \frametitle{Conclusion}

        \begin{itemize}
            \item Plus une preuve de concept qu'un programme fonctionnel pouvant être mis en production
            \item Compétences acquises et mises en oeuvre (Unity)
            \item Suite : faire ou utiliser une implémentation pour la mise en production
        \end{itemize}
    \end{frame}

    \begin{frame}
        \begin{Huge}
            \centerline{Merci}
        \end{Huge}
    \end{frame}

\end{document}
